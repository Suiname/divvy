const fs = require('fs');
const parse = require('csv-parse');

const makeData = async () => {
  return new Promise((resolve, reject) => {
    const divvyData = [];
    fs.createReadStream('data/Divvy_Trips_2019_Q2')
      .pipe(
        parse({
          delimiter: ',',
          columns: true,
        })
      )
      .on('data', row => {
        const stationId = row['02 - Rental End Station ID'];
        const tripEnd = row['01 - Rental Details Local End Time'];
        const birthday = row['05 - Member Details Member Birthday Year'];
        const id = row['01 - Rental Details Rental ID'];
        divvyData.push({
          id,
          stationId,
          tripEnd,
          birthday,
        });
      })
      .on('end', () => {
        resolve(divvyData);
      })
      .on('error', error => {
        reject(error);
      });
  });
};

module.exports = makeData;
