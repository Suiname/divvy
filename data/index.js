const parse = require('csv-parse');
const fs = require('fs');
const makeData = require('./load');

let instance = null;
class Data {
  static async getDataInstance() {
    if (!instance) {
      instance = await makeData();
    }

    return instance;
  }

  static async findAges(stations, date) {
    const ageGroups = {
      '0-20': 0,
      '21-30': 0,
      '31-40': 0,
      '41-50': 0,
      '51+': 0,
      unknown: 0,
    };

    try {
      const data = await this.getDataInstance();
      data.forEach(({ stationId, tripEnd, birthday }) => {
        const endDate = tripEnd.slice(0, 10);
        const notIncluded =
          typeof stations === 'object'
            ? !stations.includes(stationId)
            : stationId !== stations;
        const correctDate = endDate === date;
        const age = 2020 - parseInt(birthday);
        if (notIncluded || !correctDate) {
          return;
        } else if (!birthday) {
          ageGroups.unknown += 1;
        } else if (age < 21) {
          ageGroups['0-20'] += 1;
        } else if (age < 31) {
          ageGroups['21-30'] += 1;
        } else if (age < 41) {
          ageGroups['31-40'] += 1;
        } else if (age < 51) {
          ageGroups['41-50'] += 1;
        } else if (age > 51) {
          ageGroups['51+'] += 1;
        } else {
          ageGroups.unknown += 1;
        }
      });

      return ageGroups;
    } catch (error) {
      throw error;
    }
  }

  static async findTrips(stations, date) {
    const trips = {};

    const findIndex = (array, trip) => {
      let low = 0;
      let high = array.length;

      while (low < high) {
        let mid = (low + high) >>> 1;
        if (array[mid].tripEnd.slice(-8) > trip.tripEnd.slice(-8)) {
          low = mid + 1;
        } else {
          high = mid;
        }
      }
      return low;
    };

    const insertTrip = (stationId, trip) => {
      const tripArray = trips[stationId];
      if (!tripArray) {
        trips[stationId] = [trip];
      } else {
        const insertIndex = findIndex(tripArray, trip);
        tripArray.splice(insertIndex, 0, trip);
        if (tripArray.length === 21) {
          tripArray.pop();
        }
      }
    };
    try {
      const data = await this.getDataInstance();
      data.forEach(({ id, stationId, tripEnd }) => {
        const notIncluded =
          typeof stations === 'object'
            ? !stations.includes(stationId)
            : stationId !== stations;
        const correctDate = tripEnd.slice(0, 10) === date;
        !notIncluded && correctDate && insertTrip(stationId, { id, tripEnd });
      });

      return trips;
    } catch (error) {
      throw error;
    }
  }
}

module.exports = Data;
