const express = require('express');
const router = express.Router();
const apiRouter = require('./api');
const authRouter = require('./auth');

router.get('/healthcheck', (req, res, next) => res.status(200).send('OK'));

router.use('/api', apiRouter);
router.use('/auth', authRouter);

module.exports = router;
