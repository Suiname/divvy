const express = require('express');
const axios = require('axios');
const router = express.Router();

router.get('/:id', async (req, res, next) => {
  try {
    const { data } = await axios.get(
      'https://gbfs.divvybikes.com/gbfs/en/station_information.json'
    );
    const [result] = data.data.stations.filter(
      station => station.station_id === req.params.id
    );
    return result
      ? res.send(result)
      : res.status(404).send('Station Not Found');
  } catch (error) {
    console.error('error retrieving data from divvybikes.com: ', error);
    return res.status(500).send('Internal Server Error');
  }
});

module.exports = router;
