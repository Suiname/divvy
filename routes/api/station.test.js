const axios = require('axios');
const stationRouter = require('./station');
jest.mock('axios');

const request = require('supertest');
const express = require('express');
const app = express();

app.use(express.urlencoded({ extended: false }));
app.use('/station', stationRouter);

let stationTwo;
let stationThree;
let divvyJSON;
let response;

beforeEach(() => {
  stationTwo = {
    short_name: '15541',
    eightd_has_key_dispenser: false,
    rental_methods: ['TRANSITCARD', 'KEY', 'CREDITCARD'],
    eightd_station_services: [],
    rental_uris: {
      android: 'https://chi.lft.to/lastmile_qr_scan',
      ios: 'https://chi.lft.to/lastmile_qr_scan',
    },
    lon: -87.62054800987242,
    electric_bike_surcharge_waiver: false,
    name: 'Buckingham Fountain',
    capacity: 39,
    station_type: 'classic',
    lat: 41.87651122881695,
    external_id: 'a3a36d9e-a135-11e9-9cda-0a87ae2ba916',
    has_kiosk: true,
    station_id: '2',
  };
  stationThree = {
    short_name: '15544',
    eightd_has_key_dispenser: false,
    rental_methods: ['TRANSITCARD', 'KEY', 'CREDITCARD'],
    eightd_station_services: [],
    rental_uris: {
      android: 'https://chi.lft.to/lastmile_qr_scan',
      ios: 'https://chi.lft.to/lastmile_qr_scan',
    },
    lon: -87.6153553902,
    electric_bike_surcharge_waiver: false,
    name: 'Shedd Aquarium',
    capacity: 55,
    station_type: 'classic',
    lat: 41.86722595682,
    external_id: 'a3a37378-a135-11e9-9cda-0a87ae2ba916',
    has_kiosk: true,
    station_id: '3',
  };
  divvyJSON = {
    data: {
      stations: [stationTwo, stationThree],
    },
  };
  response = { data: divvyJSON };
  axios.get.mockResolvedValue(response);
});

test('finds station 2 by id', done => {
  request(app)
    .get('/station/2')
    .expect('Content-Type', /json/)
    .expect(stationTwo)
    .expect(200, done);
});

test('finds station 3 by id', done => {
  request(app)
    .get('/station/3')
    .expect('Content-Type', /json/)
    .expect(stationThree)
    .expect(200, done);
});

test("Returns a 404 when station can't be found", done => {
  request(app)
    .get('/station/1')
    .expect('Content-Type', /text\/html/)
    .expect('Station Not Found')
    .expect(404, done);
});

test('Returns a 500 when an error is thrown', done => {
  axios.get.mockResolvedValue(new Error('Mock Error'));
  request(app)
    .get('/station/2')
    .expect('Content-Type', /text\/html/)
    .expect('Internal Server Error')
    .expect(500, done);
});
