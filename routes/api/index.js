const express = require('express');
const router = express.Router();
const stationRouter = require('./station');
const stationsRouter = require('./stations');

router.get('/test', (req, res, next) => res.status(200).send('OK'));
router.use('/station', stationRouter);
router.use('/stations', stationsRouter);

module.exports = router;
