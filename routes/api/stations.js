const express = require('express');
const router = express.Router();
const Data = require('../../data');

const paramCheck = (ids, date) => {
  if (!ids && !ids.length)
    return { status: 400, message: 'Station Ids Required' };
  if (!date) return { status: 400, message: 'Date required' };
  const dateRegEx = /^\d{4}-\d{2}-\d{2}$/;
  if (!date.match(dateRegEx))
    return { status: 400, message: 'Date string format must be YYYY-MM-DD' };
  return null;
};

router.get('/age', async (req, res, next) => {
  const { ids, date } = req.query;
  const error = paramCheck(ids, date);
  if (error) return res.status(error.status).send(error.message);
  try {
    const ageGroups = await Data.findAges(ids, date);
    return res.status(200).send(ageGroups);
  } catch (error) {
    console.error('error finding age data: ', error);
    return res.status(500).send('Internal Server Error');
  }
});

router.get('/trips', async (req, res, next) => {
  const { ids, date } = req.query;
  const error = paramCheck(ids, date);
  if (error) return res.status(error.status).send(error.message);
  try {
    const trips = await Data.findTrips(ids, date);
    return res.status(200).send(trips);
  } catch (error) {
    console.error('error finding trip data: ', error);
    return res.status(500).send('Internal Server Error');
  }
});

module.exports = router;
