const express = require('express');
const router = express.Router();
const JWT = require('jsonwebtoken');

const validApiKeys = ['API4YUM', 'MYAPIKEY'];
const apiKeyHash = {
  API4YUM: 'Yum! Brands',
  MYAPIKEY: 'Jason Tham',
};

router.post('/authenticate', (req, res) => {
  if (!req.body.apiKey) return res.status(400).send('Missing apiKey');
  if (!validApiKeys.includes(req.body.apiKey))
    return res.status(401).send('Invalid API Key');
  try {
    const token = JWT.sign(
      { user: apiKeyHash[req.body.apiKey] },
      'divvy-secret'
    );
    return res.send({ token });
  } catch (error) {
    console.error('error issuing JWT: ', error);
    return res.status(500).send('Internal Server Error');
  }
});

module.exports = router;
