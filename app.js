const express = require('express');
const path = require('path');
const jwt = require('express-jwt');
const indexRouter = require('./routes/index');
const Data = require('./data');

exports.appPromise = Data.getDataInstance().then(() => {
  const app = express();

  app.use(express.json());
  app.use(
    jwt({ secret: 'divvy-secret', algorithms: ['HS256'] }).unless({
      path: ['/healthcheck', /auth\/.*/],
    })
  ); // leave healthcheck and auth routes unprotected by JWT
  app.use(express.urlencoded({ extended: false }));
  app.use(express.static(path.join(__dirname, 'public')));

  app.use('/', indexRouter);
  return app;
});
