# README for Yum! Node.js Coding Challenge

## Local Setup
I create a node (v12) / express application and have dockerized it.  During development I generally ran the application with docker compose.  
To get the app up and running you can run these commands in sequence:
```
docker-compose down
docker-compose build
docker-compose up
```

If you prefer to just run the application locally via node without docker, you can just run these commands:
```
npm install
npm start
```

The app takes about 10 seconds to load as it is loading the divvy data into memory (notes on this in the last section of the README), but should then be available at http://localhost:3000, though authentication will be needed to hit any api endpoints.

## API Endpoints
In order to access any of the API endpoints, you will first need to get a JWT token and set it as a bearer token in the Authorization header.  If you do not present a valid JWT token, the API endpoints will send back 401 Unauthorized error.

To retrieve a JWT, make a POST request (I generally used postman during development) to `/auth/authenticate` with either a JSON object or form url encoded request that has key of `apiKey` and value of `API4YUM`.  Example JSON object:
```
{
	"apiKey": "API4YUM"
}
```
Your JWT will be returned as a string value in a JSON object like so:
```
{
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiWXVtISBCcmFuZHMiLCJpYXQiOjE1OTM1MDA0MjZ9.xeD_p1emW8LZOrL9evWZh3qV0hl4PnOTu_RbBXnJYxs"
}
```
In order to access any of the API endpoints, this token must be attached to the Authorization header using the Bearer schema.  Here is an example of a request doing so in Postman:
![](token.png)

Three main endpoints were given for this code challenge.  They are described in detail below:

### Return the information for one station given a station id
This endpoint is available at route GET `/api/station/:id`.  The id of the station is passed via route parameter and the data returned is a JSON object from the divvy bikes station information [hosted here](https://gbfs.divvybikes.com/gbfs/en/station_information.json).


### Given one or more stations, return the number of riders in the following age groups,[0-20,21-30,31-40,41-50,51+, unknown], who ended their trip at that station for a given day.

This endpoint is available at route GET `/api/stations/age`.  The endpoint expects query parameters of `ids` and `date`.  `ids` can be either a single id or an array of ids.  `date` must be a date string formatted in the `YYYY-MM-DD` format.  For example, if you wanted the data for stations 203 and 204 on April 2nd 2019, your request URL would be: `/api/stations/age?ids=203&ids=204&date=2019-04-02`.  The endpoint will return a JSON object with the age groups as keys and the total riders in those age groups as values like so:
```
{
    "0-20": 0,
    "21-30": 3,
    "31-40": 2,
    "41-50": 3,
    "51+": 4,
    "unknown": 0
}
```

### Given one or more stations, return the last 20 trips that ended at each station for a single day.
This endpoint is available at route GET `/api/stations/trips`.  This endpoint also expects query parameters of `ids` and `date`.  `ids` can again be either a single id or an array of ids.  `date` must be a date string formatted in the `YYYY-MM-DD` format.  Again, if you wanted the data for stations 203 and 204 on April 2nd 2019, your request URL would be: `/api/stations/trips?ids=203&ids=204&date=2019-04-02`.  The endpoint will return a JSON object with the station ID as keys and the values are an array of JSON objects which contain the trip id and trip end timestamp sorted by most recent.  An example of a response is:
```
{
    "202": [
        {
            "id": "22197027",
            "tripEnd": "2019-04-02 20:08:18"
        },
        {
            "id": "22196982",
            "tripEnd": "2019-04-02 19:48:23"
        },
        {
            "id": "22196833",
            "tripEnd": "2019-04-02 19:31:50"
        },
        {
            "id": "22196645",
            "tripEnd": "2019-04-02 19:30:40"
        },
        {
            "id": "22193488",
            "tripEnd": "2019-04-02 16:45:52"
        },
        {
            "id": "22193242",
            "tripEnd": "2019-04-02 16:11:52"
        },
        {
            "id": "22193111",
            "tripEnd": "2019-04-02 16:03:55"
        },
        {
            "id": "22191340",
            "tripEnd": "2019-04-02 11:59:56"
        },
        {
            "id": "22189538",
            "tripEnd": "2019-04-02 08:48:58"
        }
    ],
    "203": [
        {
            "id": "22197544",
            "tripEnd": "2019-04-02 21:41:47"
        },
        {
            "id": "22193465",
            "tripEnd": "2019-04-02 17:17:50"
        },
        {
            "id": "22194186",
            "tripEnd": "2019-04-02 17:05:46"
        }
    ]
}
```

### Miscellaneous Notes
In the interest of expediency, I created the application using the express generator, which is why there is some boilerplate code.  

As far as optimization of the application, I decided to load the Divvy trip data into memory at application start because reading the data from a file stream on every request makes each request take about 10 seconds.  This makes it so the application does require some startup time to load the data into memory on boot, but the requests went from 10 seconds to around 100ish milliseconds.  The instructions did say not to use npm caching modules, but I assumed that loading the file into memory using the native node API would be fine.  I didn't do this with the divvy station data because even though I am fetching that JSON data every time, the requests still only take around 200 ms, so even though I could optimize it by storing the data in memory, the amount of performance gained is pretty low so I didn't bother.

As far as security and such is concerned, I didn't really harden the application at all since that wasn't a requirement.  If I wanted to harden it, I would probably at least hash the API keys and store the secrets for generating the JWTs in an encrypted way.  This is of course easier to do with a real database as well.

I wrote a unit test for the `station/:id` route using Jest and the supertest package, which is commonly used for testing express.  You can run the test using command `npm test`, which should run the Jest test runner and report code coverage.

The npm packages I decided to use were axios for http requests, csv-parse to load the divvy trips data from the file, express-jwt for an easy json web token express middleware, and jsonwebtoken to actually generate the web tokens.